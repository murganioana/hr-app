package com.cgi.hrapp.service;

import com.cgi.hrapp.model.Employee;
import com.cgi.hrapp.repository.EmployeeRepository;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;

/**
 * @author Horia-Marian Trandafir
 */
@Service

public class EmployeeExcelReader {

    @Autowired
    EmployeeRepository employeeRepository;

    @Transactional(rollbackFor = Error.class)
    public void read(InputStream fis) throws BiffException, ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
        Workbook workbook = null;


        try {

            workbook = Workbook.getWorkbook(fis);

            Sheet sheet = workbook.getSheet(0);

            System.out.println("rows este " + sheet.getRows());
            for (int i = 1; i < sheet.getRows(); i++) {


                Cell[] row = sheet.getRow(i);

                Employee employee = new Employee();

                String primId = row[0].getContents();
                if (primId.equals("")) {
                    break;
                }
                employee.setId(Integer.parseInt(primId));

                employee.setAddress(row[1].getContents());

                String birthDateString = row[2].getContents();
                LocalDate birthDate = formatter.parse(birthDateString).toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();
                employee.setBirthDate(birthDate);

                employee.setBirthPlace(row[3].getContents());

                employee.setCnp(row[4].getContents());

                employee.setCodCOR(row[5].getContents());

                employee.setEmail(row[6].getContents());

                employee.setFirstName(row[7].getContents());

                employee.setFrenchLevel(row[8].getContents());

                String hireDateString = row[9].getContents();
                LocalDate hireDate = formatter.parse(hireDateString).toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();
                employee.setHireDate(hireDate);

                employee.setLastName(row[10].getContents());

                employee.setManager(row[11].getContents());

                employee.setPhoneNumber("0" + row[12].getContents());

                employee.setSalary(Integer.parseInt(row[13].getContents()));

                employeeRepository.save(employee);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            if (workbook != null) {
                workbook.close();
            }
        }

    }

}
