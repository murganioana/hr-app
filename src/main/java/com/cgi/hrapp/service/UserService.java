package com.cgi.hrapp.service;

import com.cgi.hrapp.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserService {
    User findUserByEmail(String email);
    User findUserByConfirmationToken(String confirmationToken);
    void saveUser(User user);
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}
