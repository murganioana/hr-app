package com.cgi.hrapp.service;

import com.cgi.hrapp.model.Candidate;
import com.cgi.hrapp.model.CandidatePlotter;

import java.util.List;

/**
 * @author Horia-Marian Trandafir
 */
public interface CandidateService {
    List<Candidate> getAllCandidates();

    void addCandidate(Candidate candidate);

    void addCandidates(List<Candidate> candidates);

    List<CandidatePlotter> calculateAverageSalariesByPosition(String position);

    List<String> getAllCandidatePositions(List<Candidate> candidates);

    List<CandidatePlotter> plotCandidates();
}
