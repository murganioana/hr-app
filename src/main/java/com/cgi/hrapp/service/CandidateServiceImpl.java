package com.cgi.hrapp.service;

import com.cgi.hrapp.model.Candidate;
import com.cgi.hrapp.model.CandidateHelper;
import com.cgi.hrapp.model.CandidatePlotter;
import com.cgi.hrapp.model.Employee;
import com.cgi.hrapp.repository.CandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CandidateServiceImpl implements CandidateService {



    @Autowired
    private CandidateRepository candidateRepository;

    public List<Candidate> getAllCandidates () {

        return candidateRepository.findAll();

    }

    public void addCandidate(Candidate candidate){

        candidateRepository.save(candidate);

    }
    public void addCandidates(List<Candidate> candidates){

        candidateRepository.saveAll(candidates);

    }
    public static boolean isBetween(Double x, Double lower, Double upper) {
        return lower <= x && x <= upper;
    }

    public List<CandidatePlotter> calculateAverageSalariesByPosition(String position){

        List<Candidate> candidates = getAllCandidates();
        List<CandidateHelper> candidateHelpers = new ArrayList<>();
        for (Candidate candidate : candidates) {
            String candidatePosition = candidate.getPosition().trim();
            if(candidatePosition.equalsIgnoreCase(position.trim())) {
                CandidateHelper candidateHelper = new CandidateHelper();
                candidateHelper.setYearsExperience(candidate.getYearsExperience());
                candidateHelper.setSalary(candidate.getDesiredSalary());
                candidateHelpers.add(candidateHelper);
            }
        }
        Double[] average = {0.00d, 0.00d, 0.00d, 0.00d, 0.00d, 0.00d, 0.00d};
        Double[] numberOfCandidates = {0.00d, 0.00d, 0.00d, 0.00d, 0.00d, 0.00d, 0.00d};
        for (CandidateHelper candidateHelper : candidateHelpers) {
            Double i=candidateHelper.getYearsExperience();
            if(isBetween(i,0d,1d)){
                average[0] += candidateHelper.getSalary();
                numberOfCandidates[0] ++;
            }
            else if (isBetween(i,1d,2d)){
                average[1] += candidateHelper.getSalary();
                numberOfCandidates[1] ++;
            }
            else if (isBetween(i,2d,4d)){
                average[2] += candidateHelper.getSalary();
                numberOfCandidates[2] ++;
            }
            else if (isBetween(i,4d,6d)){
                average[3] += candidateHelper.getSalary();
                numberOfCandidates[3] ++;
            }
            else if (isBetween(i,6d,10d)){
                average[4] += candidateHelper.getSalary();
                numberOfCandidates[4] ++;
            }
            else if (isBetween(i,10d,15d)){
                average[5] += candidateHelper.getSalary();
                numberOfCandidates[5] ++;
            }
            else {
                average[6] += candidateHelper.getSalary();
                numberOfCandidates[6] ++;
            }
        }
            for (int j = 0; j < 7; j++) {
                if(average[j]!=0.00d)
                    average[j] = average[j]/numberOfCandidates[j];
                else average[j]=0.00d;
            }



//        List<HashMap<T,T>> rtrn = new ArrayList<>();
//        HashMap<String,String> columns = new HashMap<>();
//        columns.put("Experience","Salary");
        String[] experienceLevels = {"<1","1-2","2-4","4-6","6-10","10-15",">15"};
        List<CandidatePlotter> candidatePlotters = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            CandidatePlotter candidatePlotter = new CandidatePlotter();
            candidatePlotter.setExperience(experienceLevels[i]);
            candidatePlotter.setSalary(average[i]);
            candidatePlotter.setPosition(position);
            candidatePlotters.add(candidatePlotter);
        }

//        HashMap<String,Object> averageSalariesByYearsOfExperience = new HashMap<>();
//        averageSalariesByYearsOfExperience.put("Experience","Salary");
//        averageSalariesByYearsOfExperience.put("<1",average[0]);
//        averageSalariesByYearsOfExperience.put("1-2",average[1]);
//        averageSalariesByYearsOfExperience.put("2-4",average[2]);
//        averageSalariesByYearsOfExperience.put("4-6",average[3]);
//        averageSalariesByYearsOfExperience.put("6-10",average[4]);
//        averageSalariesByYearsOfExperience.put("10-15",average[5]);
//        averageSalariesByYearsOfExperience.put(">15",average[6]);


        return candidatePlotters;
    }

    public List<String> getAllCandidatePositions (List<Candidate> candidates){
        List<String> allPositions = new ArrayList<>();
        for (Candidate candidate : candidates) {
            allPositions.add(candidate.getPosition());
        }
        Set<String> differentPositions = new HashSet<>(allPositions);
        return new ArrayList<>(differentPositions);
    }

    public List<CandidatePlotter> plotCandidates(){
        List<Candidate> candidates = candidateRepository.findAll();
        List<String> positions = getAllCandidatePositions(candidates);
        List<CandidatePlotter> candidatePlotters = new ArrayList<>();
        for (String position : positions) {
            candidatePlotters.addAll(calculateAverageSalariesByPosition(position));
        }
        return candidatePlotters;
    }

}
