package com.cgi.hrapp.service;


import com.cgi.hrapp.model.Candidate;
import com.cgi.hrapp.repository.CandidateRepository;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;

/**
 * @author Horia-Marian Trandafir
 */
@Service
public class CandidateExcelReader {

    @Autowired
    CandidateRepository candidateRepository;

    private LocalDate stringToLocalDate(String s) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
        return formatter.parse(s).toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    @Transactional(rollbackFor = Error.class)
    public void read(InputStream fis) throws BiffException, ParseException {


        Workbook workbook = null;


        try {

            workbook = Workbook.getWorkbook(fis);

            Sheet sheet = workbook.getSheet(0);

            System.out.println("nr entryuri " + sheet.getRows());
            for (int i = 1; i < sheet.getRows(); i++) {

                Cell[] row = sheet.getRow(i);

                Candidate candidate = new Candidate();

                String primId = row[0].getContents();
                if (primId.equals("")) {
                    break;
                }
                candidate.setId(Integer.parseInt(primId));

                candidate.setAddress(row[1].getContents());

                candidate.setBirthDate(stringToLocalDate(row[2].getContents()));

                candidate.setComments(row[3].getContents());

                candidate.setDateContractSign(stringToLocalDate(row[4].getContents()));

                candidate.setDateHRInterview(stringToLocalDate(row[5].getContents()));

                candidate.setDateStartProcess(stringToLocalDate(row[6].getContents()));

                candidate.setDateTechnicalInterview(stringToLocalDate(row[7].getContents()));

                candidate.setDecision(row[8].getContents());

                candidate.setDesiredSalary(Integer.parseInt(row[9].getContents()));

                candidate.setEmail(row[10].getContents());

                candidate.setFeedbackHRInterview(row[11].getContents());

                candidate.setFeedbackTechnicalInterview(row[12].getContents());

                candidate.setFirstName(row[13].getContents());

                candidate.setFrenchLevel(row[14].getContents());

                candidate.setLastName(row[15].getContents());

                candidate.setLevel(row[16].getContents());

                candidate.setPhoneNumber(row[17].getContents());

                candidate.setPosition(row[18].getContents());

                candidate.setReasonsOfRecruitment(row[19].getContents());

                candidate.setRefusalReason(row[20].getContents());

                candidate.setResponsibleHRInterview(row[21].getContents());

                candidate.setResponsibleTechnicalInterview(row[22].getContents());

                candidate.setSalaryProposed(Integer.parseInt(row[23].getContents()));

                candidate.setSource(row[24].getContents());

                candidate.setStartDate(stringToLocalDate(row[25].getContents()));

                candidate.setStatus(row[26].getContents());

                candidate.setTaxDeductionEligibility(row[27].getContents());

                candidate.setYearsExperience(Double.parseDouble(row[28].getContents()));

                candidateRepository.save(candidate);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            if (workbook != null) {
                workbook.close();
            }
        }

    }
}
