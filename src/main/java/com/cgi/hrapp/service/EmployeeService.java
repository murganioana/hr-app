package com.cgi.hrapp.service;

import com.cgi.hrapp.model.Employee;
import com.cgi.hrapp.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;


    public List<Employee> getAllEmployees() {

        return employeeRepository.findAll();

    }

    public void addEmployee(Employee employee) {

        employeeRepository.save(employee);

    }

    public void addEmployees(List<Employee> employees) {

        employeeRepository.saveAll(employees);

    }

    public Employee findEmployeeById(Integer id) {

        return employeeRepository.findEmployeeById(id);
    }

}
