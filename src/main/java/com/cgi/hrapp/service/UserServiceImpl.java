package com.cgi.hrapp.service;

import com.cgi.hrapp.model.User;
import com.cgi.hrapp.repository.RoleRepository;
import com.cgi.hrapp.repository.UserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;


@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Override
    public User findUserByConfirmationToken(String confirmationToken) {
        return userRepository.findUserByConfirmationToken(confirmationToken);
    }

    @Override
    public void saveUser(User user) {

        //aici putem alege cine e admin si cine e user
        //putem avea o lista in fisierele de proprietati cu adresele adminilor
        /*if (user.isEnabled()) {
            Role userRole = roleRepository.findByRoleName("ADMIN");
            user.setRoles(new HashSet<>(Arrays.asList(userRole)));
        }*/
        userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findUserByEmail(username);

        Set<GrantedAuthority> grantedAuthorities = user.getAuthorities().stream().collect(Collectors.toSet());

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), grantedAuthorities);
    }
}
