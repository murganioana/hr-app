package com.cgi.hrapp.service;

import com.cgi.hrapp.model.Role;

/**
 * @author Horia-Marian Trandafir
 */
public interface RoleService {
    Role findRoleByRoleName(String roleName);
}
