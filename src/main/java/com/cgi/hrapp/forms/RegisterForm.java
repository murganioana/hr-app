package com.cgi.hrapp.forms;

import javax.validation.constraints.Email;

/**
 * @author Horia-Marian Trandafir
 */

public class RegisterForm {

    public String firstName;
    public String lastName;
    @Email
    public String email;
    public String roleName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
