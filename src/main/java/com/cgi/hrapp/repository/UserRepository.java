package com.cgi.hrapp.repository;

import com.cgi.hrapp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,String> {

    User findUserByEmail(String userName);
    User findUserByConfirmationToken(String token);
}
