package com.cgi.hrapp.controller;

import com.cgi.hrapp.model.Candidate;
import com.cgi.hrapp.model.CandidatePlotter;
import com.cgi.hrapp.service.CandidateExcelReader;
import com.cgi.hrapp.service.CandidateServiceImpl;
import jxl.read.biff.BiffException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;

@Controller
public class CandidateController {

    @Autowired
    private CandidateServiceImpl candidateServiceImpl;
    @Autowired
    private CandidateExcelReader candidateExcelReader;

    @GetMapping("/candidates/{position}")
    @ResponseBody
    public List<CandidatePlotter> plotCandidate(@PathVariable("position") String position) {
        return candidateServiceImpl.calculateAverageSalariesByPosition(position);
    }

    @GetMapping("/showCandidatesCharts")
    public String getAllCandidatesChart() {

        return "plottest3";
    }

    @GetMapping("/candidates/plot")
    @ResponseBody
    public List<CandidatePlotter> plotCandidates() {
        return candidateServiceImpl.plotCandidates();
    }

    @GetMapping("/candidates")
    public String getAllCandidates(ModelMap modelMap) {

        modelMap.put("candidates", candidateServiceImpl.getAllCandidates());
        return "candidates";
    }

    @PostMapping("/candidate/updateCandidate")
    public String updateEmployee(@ModelAttribute("candidateUpdated") Candidate candidate) {

        candidateServiceImpl.addCandidate(candidate);

        return "index";
    }

    @GetMapping("/candidates/editCandidate/{id}")
    public String editEmployee(@PathVariable("id") Integer id, ModelMap model) {
        model.put("candidate", "");
        return "editCandidate";
    }

    @PostMapping("/addCandidates")
    public void addCandidates(@RequestBody List<Candidate> candidates) {

        candidateServiceImpl.addCandidates(candidates);
    }


    @PostMapping("/candidates/upload")
    public void candidatesUpload(@RequestParam("inputCandidateFile") MultipartFile file, HttpServletResponse res) {

        if (file.isEmpty()) {
            res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

        try {
            InputStream fis = file.getInputStream();
            candidateExcelReader.read(fis);

        } catch (IOException | BiffException e) {
            res.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
            e.printStackTrace();
        } catch (ParseException e) {
            res.setStatus(HttpServletResponse.SC_CONFLICT);//input date has wrong format
            e.printStackTrace();
        }

        res.setStatus(HttpServletResponse.SC_NO_CONTENT);
    }
}
