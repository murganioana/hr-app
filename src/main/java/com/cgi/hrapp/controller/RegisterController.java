package com.cgi.hrapp.controller;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.cgi.hrapp.forms.RegisterForm;
import com.cgi.hrapp.model.Role;
import com.cgi.hrapp.model.User;
import com.cgi.hrapp.service.EmailService;
import com.cgi.hrapp.service.RoleService;
import com.cgi.hrapp.service.UserService;
import com.cgi.hrapp.service.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class RegisterController {
	
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private UserService userService;
	private EmailService emailService;
	private RoleService roleService;
    private AuthenticationManager authenticationManager;

    private Logger logger = LoggerFactory.getLogger(RegisterController.class);

	@Autowired
	public RegisterController(BCryptPasswordEncoder bCryptPasswordEncoder,
							  UserService userService, EmailService emailService, RoleService roleService, AuthenticationManager authenticationManager) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.userService = userService;
		this.emailService = emailService;
		this.roleService = roleService;
		this.authenticationManager = authenticationManager;
	}
	
	// Return registration form template
	@RequestMapping(value="/register", method = RequestMethod.GET)
	public ModelAndView showRegistrationPage(){
		ModelAndView modelAndView = new ModelAndView();
		RegisterForm registerForm = new RegisterForm();
		modelAndView.addObject("registerForm", registerForm);
		modelAndView.setViewName("register");
		return modelAndView;
	}

	// Process form input data
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView processRegistrationForm(@Valid RegisterForm registerForm,
												BindingResult bindingResult, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();

		// Lookup user in database by e-mail
		User userExists = userService.findUserByEmail(registerForm.getEmail());

		System.out.println(userExists);

		if (userExists != null) {
			modelAndView.addObject("alreadyRegisteredMessage", "Oops!  There is already a user registered with the email provided.");
			modelAndView.setViewName("register");
			bindingResult.reject("email");
		}

		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("register");
		} else { // new user so we create user and send confirmation e-mail

			User user = new User();
			// Disable user until they click on confirmation link in email
			user.setEnabled(false);
			// Generate random 36-character string token for confirmation link
			user.setConfirmationToken(UUID.randomUUID().toString());
			user.setFirstName(registerForm.getFirstName());
			user.setLastName(registerForm.getLastName());
			user.setEmail(registerForm.getEmail());

			//System.out.println(registerForm.getRoleName());
			Set<Role> roles = new HashSet<>();
			String roleName = registerForm.getRoleName();
			Role role = roleService.findRoleByRoleName(roleName);
			roles.add(role);

			user.setRoles(roles);
			userService.saveUser(user);

			String appUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();

			SimpleMailMessage registrationEmail = new SimpleMailMessage();
			registrationEmail.setTo(user.getEmail());
			registrationEmail.setSubject("Registration Confirmation");
			registrationEmail.setText("To confirm your e-mail address, please click the link below:\n"
					+ appUrl + "/confirm?token=" + user.getConfirmationToken());
			registrationEmail.setFrom("noreply@domain.com");

			emailService.sendEmail(registrationEmail);

			modelAndView.addObject("confirmationMessage", "A confirmation e-mail has been sent to " + user.getEmail());
			modelAndView.setViewName("register");
		}

		return modelAndView;
	}
	
	// Process confirmation link
	@RequestMapping(value="/confirm", method = RequestMethod.GET)
	public ModelAndView confirmRegistration(ModelAndView modelAndView, @RequestParam("token") String token) {
			
		User user = userService.findUserByConfirmationToken(token);
			
		if (user == null) { // No token found in DB
			modelAndView.addObject("invalidToken", "Oops!  This is an invalid confirmation link.");
            logger.warn("Invalid token [{}]", token);
		} else { // Token found
			modelAndView.addObject("confirmationToken", user.getConfirmationToken());
            logger.info("New user registered [{}]", user.getFirstName(), user.getLastName());
		}
			
		modelAndView.setViewName("confirm");
		return modelAndView;		
	}
	
	// Process confirmation link
	@RequestMapping(value="/confirm", method = RequestMethod.POST)
	public ModelAndView confirmRegistration(ModelAndView modelAndView, BindingResult bindingResult, @RequestParam Map<String, String> requestParams, RedirectAttributes redir) {
				
		modelAndView.setViewName("confirm");

		// Find the user associated with the reset token
		User user = userService.findUserByConfirmationToken(requestParams.get("token"));

		// Set new password
        logger.info("Setting password for user [{}]", user.getFirstName(), user.getLastName());
		user.setPassword(bCryptPasswordEncoder.encode(requestParams.get("password")));


		// Set user to enabled
        logger.info("Enabling user [{}]", user.getFirstName(), user.getLastName());
		user.setEnabled(true);

		// Save user
        logger.info("Saving user [{}]", user.getFirstName(), user.getLastName());
        userService.saveUser(user);

        this.autologin(user.getEmail(), requestParams.get("password"));
		
		modelAndView.addObject("successMessage", "Your password has been set!");
        logger.info("User saved into database [{}]", user.getFirstName(), user.getLastName());
		return modelAndView;		
	}

	private void autologin(String username, String password) {
        UserDetails userDetails = userService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());

        authenticationManager.authenticate(usernamePasswordAuthenticationToken);

        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            logger.info(String.format("Auto login %s successfully!", username));
        }
    }
}