package com.cgi.hrapp.controller;

import com.cgi.hrapp.model.Appointment;
import com.cgi.hrapp.model.Employee;
import com.cgi.hrapp.service.AppointmentsService;
import com.cgi.hrapp.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.validation.Valid;

@Controller
public class AppointmentController {

    private AppointmentsService appointmentsService;
    private EmployeeService employeeService;

    @Autowired
    public AppointmentController(AppointmentsService appointmentsService, EmployeeService employeeService) {
        this.appointmentsService = appointmentsService;
        this.employeeService = employeeService;
    }

    @GetMapping("/showAppointments")
    public String showAppointments(ModelMap modelMap) {

        modelMap.put("appointments", appointmentsService.getAllAppointments());
        modelMap.put("appointment", new Appointment());

        return "appointments";
    }

    @GetMapping("/showCreateAppointmentForm")
    public String showCreateAppointmentForm(Appointment appointment) {

        return "createAppointment";
    }

    @PostMapping("/createAppointment")
    public String createEmployee(@ModelAttribute("appointment") @Valid Appointment appointment, BindingResult bindingResult, RedirectAttributes redirectAttributes, ModelMap modelMap) {

        Employee employee = employeeService.findEmployeeById(appointment.getEmployeeId());

        if (bindingResult.hasErrors()) {
            System.out.println("Bindings errors ......");
            //redirectAttributes.addFlashAttribute("appointment", appointment);
            return "createAppointment";
        }
        if (employee == null) {
            modelMap.addAttribute("employeeNotExist", "Employee not existing in db");
            //redirectAttributes.addFlashAttribute("appointment", appointment);
            System.out.println("Employee not existing in db");
            return "createAppointment";
        }

        appointment.setEmployee(employee);
        appointmentsService.addAppointment(appointment);
        modelMap.addAttribute("appointmentAddSuccess", "Appointment created successfully!");
        return "appointments";
    }

    @GetMapping("/deleteAppointment/{id}")
    public String deleteAppointment(@PathVariable("id") Integer id, ModelMap modelMap) {

        appointmentsService.deleteAppointment(id);
        modelMap.put("", "");
        return "redirect:/showAppointments";
    }
}
