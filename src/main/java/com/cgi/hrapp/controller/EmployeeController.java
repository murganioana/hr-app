package com.cgi.hrapp.controller;

import com.cgi.hrapp.model.Employee;
import com.cgi.hrapp.service.EmployeeExcelReader;
import com.cgi.hrapp.service.EmployeeService;
import jxl.read.biff.BiffException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;

@Controller
@RequestMapping("/employees")
public class EmployeeController {

    private EmployeeExcelReader employeeExcelReader;
    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeExcelReader employeeExcelReader, EmployeeService employeeService){
        this.employeeExcelReader = employeeExcelReader;
        this.employeeService = employeeService;

    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getAllEmployees(ModelMap modelMap) {

        List<Employee> employees = employeeService.getAllEmployees();

        modelMap.put("employees", filterSalaryForManagers(employees));
        return "employees";
    }

    @GetMapping("/plotEmployees")
    @ResponseBody
    public List<Employee> plotEmployees(){
        return employeeService.getAllEmployees();
    }

    @GetMapping("/showCreateEmployeeForm")
    public String showCreateEmployeeForm(Employee employee) {

        return "createEmployee";
    }

    @PostMapping("/createEmployee")
    public String createEmployee(@Valid Employee employee, BindingResult bindingResult, ModelMap modelMap, RedirectAttributes redirectAttributes) {

        Employee employee1 = employeeService.findEmployeeById(employee.getId());
        if (bindingResult.hasErrors()) {
            System.out.println("Bindings errors ......");

            modelMap.addAttribute("employeeCreateError", "Employee create error !");
            return "createEmployee";
        }
        if(employee1 != null ){
            modelMap.addAttribute("employeeCreateError", "Employee already exist!");
            return "createEmployee";
        }
        modelMap.addAttribute("employeeCreated", "Employee created successfully!");
        employeeService.addEmployee(employee);
        return "index";
    }

    @PostMapping("/updateEmployee")
    public ModelAndView updateEmployee(@Valid Employee employee) {

        ModelAndView modelAndView = new ModelAndView();
        employeeService.addEmployee(employee);

        modelAndView.addObject("employeeUpdated", "Employee updated successfully!");

        modelAndView.setViewName("index");
        return modelAndView;
    }

    @GetMapping("/editEmployee/{id}")
    public String editEmployee(@PathVariable("id") Integer id, ModelMap model) {
        model.put("employee", employeeService.findEmployeeById(id));
        return "editEmployee";
    }

    @PostMapping("/upload")
    public void fileUpload(@RequestParam("inputEmployeeFile") MultipartFile file, HttpServletResponse res) {

        if (file.isEmpty()) {
            res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

        try {
            InputStream fis = file.getInputStream();
            employeeExcelReader.read(fis);

        } catch (IOException | BiffException e) {
            res.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
            e.printStackTrace();
        } catch (ParseException e) {
            res.setStatus(HttpServletResponse.SC_CONFLICT);
            e.printStackTrace();
        }

        res.setStatus(HttpServletResponse.SC_NO_CONTENT);
    }

    public List<Employee> filterSalaryForManagers(List<Employee> employees){

        boolean userIsManager = SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals("ROLE_MANAGER"));
        for (Employee employee : employees) {


            if(employee.getCodCOR().equals("251206"))
                if(userIsManager)
                    employee.setSalary(null);

        }
        return employees;
    }
}
