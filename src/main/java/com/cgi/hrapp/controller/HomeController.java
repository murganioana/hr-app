package com.cgi.hrapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping("/blank")
    public String blank(Model model) {

        return "blank";
    }
    @RequestMapping("/index")
    public String home(Model model) {

        return "index";
    }
}
