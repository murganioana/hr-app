package com.cgi.hrapp.controller;

import com.cgi.hrapp.service.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

    private AuthenticationManager authenticationManager;
    private UserServiceImpl userServiceImpl;

    @Autowired
    public LoginController (AuthenticationManager authenticationManager, UserServiceImpl userServiceImpl){
        this.authenticationManager = authenticationManager;
        this.userServiceImpl = userServiceImpl;

    }

    private Logger logger = LoggerFactory.getLogger(LoginController.class);


    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("logout", "You have been logged out successfully.");

        return "login";
    }
}
