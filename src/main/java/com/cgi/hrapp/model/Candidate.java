package com.cgi.hrapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import java.time.LocalDate;

@Entity(name = "candidate")
public class Candidate {

    @Id
    @GeneratedValue
    private Integer id;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String level;
    private String status;
    private String address;
    private String source;
    private String phoneNumber;
    @Email
    private String email;
    private String frenchLevel;
    private LocalDate dateStartProcess;
    private String position;
    private Double yearsExperience;
    private Integer desiredSalary;
    private Integer salaryProposed;
    private LocalDate dateTechnicalInterview;
    private String responsibleTechnicalInterview;
    private String feedbackTechnicalInterview;
    private LocalDate dateHRInterview;
    private String responsibleHRInterview;
    private String feedbackHRInterview;
    private String decision;
    private LocalDate startDate;
    private LocalDate dateContractSign;
    private String comments;
    private String reasonsOfRecruitment;
    private String refusalReason;
    private String taxDeductionEligibility;


    public Candidate() {
    }

    public Candidate(String firstName, String lastName, LocalDate birthDate, String level, String status, String address, String source, String phoneNumber, @Email String email, String frenchLevel, LocalDate dateStartProcess, String position, Double yearsExperience, Integer desiredSalary, Integer salaryProposed, LocalDate dateTechnicalInterview, String responsibleTechnicalInterview, String feedbackTechnicalInterview, LocalDate dateHRInterview, String responsibleHRInterview, String feedbackHRInterview, String decision, LocalDate startDate, LocalDate dateContractSign, String comments, String reasonsOfRecruitment, String refusalReason, String taxDeductionEligibility) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.level = level;
        this.status = status;
        this.address = address;
        this.source = source;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.frenchLevel = frenchLevel;
        this.dateStartProcess = dateStartProcess;
        this.position = position;
        this.yearsExperience = yearsExperience;
        this.desiredSalary = desiredSalary;
        this.salaryProposed = salaryProposed;
        this.dateTechnicalInterview = dateTechnicalInterview;
        this.responsibleTechnicalInterview = responsibleTechnicalInterview;
        this.feedbackTechnicalInterview = feedbackTechnicalInterview;
        this.dateHRInterview = dateHRInterview;
        this.responsibleHRInterview = responsibleHRInterview;
        this.feedbackHRInterview = feedbackHRInterview;
        this.decision = decision;
        this.startDate = startDate;
        this.dateContractSign = dateContractSign;
        this.comments = comments;
        this.reasonsOfRecruitment = reasonsOfRecruitment;
        this.refusalReason = refusalReason;
        this.taxDeductionEligibility = taxDeductionEligibility;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFrenchLevel() {
        return frenchLevel;
    }

    public void setFrenchLevel(String frenchLevel) {
        this.frenchLevel = frenchLevel;
    }

    public LocalDate getDateStartProcess() {
        return dateStartProcess;
    }

    public void setDateStartProcess(LocalDate dateStartProcess) {
        this.dateStartProcess = dateStartProcess;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Double getYearsExperience() {
        return yearsExperience;
    }

    public void setYearsExperience(Double yearsExperience) {
        this.yearsExperience = yearsExperience;
    }

    public Integer getDesiredSalary() {
        return desiredSalary;
    }

    public void setDesiredSalary(Integer desiredSalary) {
        this.desiredSalary = desiredSalary;
    }

    public Integer getSalaryProposed() {
        return salaryProposed;
    }

    public void setSalaryProposed(Integer salaryProposed) {
        this.salaryProposed = salaryProposed;
    }

    public LocalDate getDateTechnicalInterview() {
        return dateTechnicalInterview;
    }

    public void setDateTechnicalInterview(LocalDate dateTechnicalInterview) {
        this.dateTechnicalInterview = dateTechnicalInterview;
    }

    public String getResponsibleTechnicalInterview() {
        return responsibleTechnicalInterview;
    }

    public void setResponsibleTechnicalInterview(String responsibleTechnicalInterview) {
        this.responsibleTechnicalInterview = responsibleTechnicalInterview;
    }

    public String getFeedbackTechnicalInterview() {
        return feedbackTechnicalInterview;
    }

    public void setFeedbackTechnicalInterview(String feedbackTechnicalInterview) {
        this.feedbackTechnicalInterview = feedbackTechnicalInterview;
    }

    public LocalDate getDateHRInterview() {
        return dateHRInterview;
    }

    public void setDateHRInterview(LocalDate dateHRInterview) {
        this.dateHRInterview = dateHRInterview;
    }

    public String getResponsibleHRInterview() {
        return responsibleHRInterview;
    }

    public void setResponsibleHRInterview(String responsibleHRInterview) {
        this.responsibleHRInterview = responsibleHRInterview;
    }

    public String getFeedbackHRInterview() {
        return feedbackHRInterview;
    }

    public void setFeedbackHRInterview(String feedbackHRInterview) {
        this.feedbackHRInterview = feedbackHRInterview;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getDateContractSign() {
        return dateContractSign;
    }

    public void setDateContractSign(LocalDate dateContractSign) {
        this.dateContractSign = dateContractSign;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getReasonsOfRecruitment() {
        return reasonsOfRecruitment;
    }

    public void setReasonsOfRecruitment(String reasonsOfRecruitment) {
        this.reasonsOfRecruitment = reasonsOfRecruitment;
    }

    public String getRefusalReason() {
        return refusalReason;
    }

    public void setRefusalReason(String refusalReason) {
        this.refusalReason = refusalReason;
    }

    public String getTaxDeductionEligibility() {
        return taxDeductionEligibility;
    }

    public void setTaxDeductionEligibility(String taxDeductionEligibility) {
        this.taxDeductionEligibility = taxDeductionEligibility;
    }
}
