package com.cgi.hrapp.model;

/**
 * @author Horia-Marian Trandafir
 */
public class CandidateHelper {

    private Double yearsExperience;
    private Integer salary;

    public Double getYearsExperience() {
        return yearsExperience;
    }

    public void setYearsExperience(Double yearsExperience) {
        this.yearsExperience = yearsExperience;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}

