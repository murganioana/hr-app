package com.cgi.hrapp.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity(name = "employee")
public class Employee {

    @Id
    @Max(999999)
    private Integer id;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String birthPlace;
    private String address;
    private String cnp;
    private String phoneNumber;
    @Email
    private String email;
    private String frenchLevel;
    private LocalDate hireDate;
    @Max(Integer.MAX_VALUE)
    private Integer salary;
    private String manager;
    @Size(min = 6 , max = 6)
    private String codCOR;

    public Employee() {
    }

    public Employee(Integer id, String firstName, String lastName, LocalDate birthDate, String birthPlace, String address, String cnp, String phoneNumber, @Email String email, String frenchLevel, LocalDate hireDate, Integer salary, String manager, String codCOR) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.birthPlace = birthPlace;
        this.address = address;
        this.cnp = cnp;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.frenchLevel = frenchLevel;
        this.hireDate = hireDate;
        this.manager = manager;
        this.salary = salary;
        this.codCOR = codCOR;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {

        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getHireDate() {
        return hireDate;
    }

    public void setHireDate(LocalDate hireDate) {
        this.hireDate = hireDate;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFrenchLevel() {
        return frenchLevel;
    }

    public void setFrenchLevel(String frenchLevel) {
        this.frenchLevel = frenchLevel;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getCodCOR() {
        return codCOR;
    }

    public void setCodCOR(String codCOR) {
        this.codCOR = codCOR;
    }

}
