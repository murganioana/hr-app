package com.cgi.hrapp.model;

/**
 * @author Horia-Marian Trandafir
 */
public class CandidatePlotter {

    private String experience;
    private Double salary;
    private String position;

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
